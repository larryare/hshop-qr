# Nintendolife hShop QR code injector
![](https://kuma.benzas.lt/api/badge/4/status?style=for-the-badge)

Preview


![preview](https://i.imgur.com/9xHDjVu.png)

## We did it boyyyyyyyyys
- **MOZZILA APPROVED THE ADDON**, get it here https://addons.mozilla.org/en-US/firefox/addon/hshop-qr/
- I'm currently hosting the apie server, uptime not guaranteed, check it [here](https://kuma.benzas.lt/status/hshopqr) or at the top of the page.



## FAQ
**What is this?**

Firefox addon + api server (**IF ANYONE DOESN'T HATE JS AS MUCH AS ME PLS REWRITE QR CODE PARSER TO JS SO ALL CODE COULD LIVE INSIDE THE EXTENSION**) to add QR codes to nintendolife.com 3ds game pages.

**Why?**

I wanted to see boxart and screenshots while browsing for games. Now you can browse for games and scan hShop qr codes right away 📸. 


## Development


- `git clone`
- enable developer options in firefox' addons setting
- load temporary addon
	- to make it permanent go to about:config and set xpinstall.signatures.required to `false`
	- *I might submit this addon to firefox store **but more likely won't***
- I use uBlock with defaults and built this accordingly, if you have problems, feel free to PR.
- build and run API server
	- download go
	- `go build`
	- `./yourbinaryexecutable`
