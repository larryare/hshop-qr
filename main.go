package main

import (
	"net/http"
	"strings"

	"github.com/anaskhan96/soup"
	"github.com/gin-gonic/gin"
)

func search(title string) string {
	searchUrl := "https://hshop.erista.me/search/results?i=games.europe%2Cgames.north-america&q=" + strings.ReplaceAll(title, " ", "+") + "&qt=Text&lgy=false"
	resp, err := soup.Get(searchUrl)
	if err != nil {
		println(err)
	}
	doc := soup.HTMLParse(resp)
	id := doc.FindStrict("a", "class", "list-entry block-link").Attrs()["href"]
	return strings.Replace(id, "/t/", "", -1)
}

func get_qr_code(id string) string {
	resp, err := soup.Get("https://hshop.erista.me/t/" + id)
	if err != nil {
		println(err)
	}
	doc := soup.HTMLParse(resp)
	return doc.FindStrict("img", "alt", "The QR Code used to download content.").Attrs()["src"]
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.GET("/getcode", func(c *gin.Context) {
		title := c.DefaultQuery("title", "none")
		qr_url := get_qr_code(search(title))
		println(title)
		println(qr_url)
		c.Redirect(http.StatusFound, qr_url)
	})
	r.Run()
}
